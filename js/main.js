const reviews = [
    {
        img : 'https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883334/person-1_rfzshl.jpg',
        name : 'persone-1',
        job : 'Developer',
        text : 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloribus, consectetur.'
    },
    {
        img : 'https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883409/person-2_np9x5l.jpg',
        name : 'persone-2',
        job : 'UI/UX',
        text : 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloribus, consectetur.'
    },
    {
        img : 'https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883417/person-3_ipa0mj.jpg',
        name : 'persone-3',
        job : 'Django',
        text : 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloribus, consectetur.'
    },
    {
        img : 'https://res.cloudinary.com/diqqf3eq2/image/upload/v1586883423/person-4_t9nxjt.jpg',
        name : 'persone-4',
        job : 'PHP',
        text : 'Lorem ipsum, dolor sit amet consectetur adipisicing elit. Doloribus, consectetur.'
    }
]

const prevBtn = document.querySelector('.prev-btn');
const nextBtn = document.querySelector('.next-btn');
const randomBtn = document.querySelector('.random-btn');

const personImage = document.querySelector('.person-img');
const personName = document.getElementById('person-name');
const personJob = document.getElementById('person-job');
const personReview = document.getElementById('person-review');

let currentItem = 0;

function showPerson(person){
    let item = reviews[person];

    personImage.src = item.img;
    personName.textContent = item.name;
    personJob.textContent = item.job;
    personReview.textContent = item.text;
}

showPerson(currentItem);

prevBtn.addEventListener('click',function(){
    currentItem--;
    if(currentItem < 0){
        currentItem = reviews.length-1;
    }
    showPerson(currentItem);
})
nextBtn.addEventListener('click',function(){
    currentItem++;
    if(currentItem > reviews.length -1){
        currentItem = 0;
    }
    showPerson(currentItem);
})
randomBtn.addEventListener('click',function(){
    currentItem = Math.floor(Math.random() * reviews.length);
    showPerson(currentItem)
})